# Setup
before opening proyect please run on the command line `pod install`

## Screenshots
### Portrait
![portrait](screenshots/portrait.png)

### Landscape
![landscape](screenshots/landscape.png)

### Table
![Table](screenshots/table.png)
