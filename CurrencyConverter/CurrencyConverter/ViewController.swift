//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by Luis Cabarique on 4/26/18.
//  Copyright © 2018 Luis Cabarique. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

enum CurrencyField{
    case current
    case convertion
}

class ViewController: UIViewController {
    var disposeBag: DisposeBag = DisposeBag()
    //MARK: Subjects
    fileprivate var nextNumberSubject = PublishSubject<Int>()
    fileprivate var deleteNumberSubject = PublishSubject<Void>()
    fileprivate var addSeparatorSubject = PublishSubject<Void>()
    fileprivate var exchangeCurrencySubject = PublishSubject<Currency>()
    
    //Outlets
    @IBOutlet weak var oneKeyView: UIView!
    @IBOutlet weak var twoKeyView: UIView!
    @IBOutlet weak var threeKeyView: UIView!
    @IBOutlet weak var fourKeyView: UIView!
    @IBOutlet weak var fiveKeyView: UIView!
    @IBOutlet weak var sixKeyView: UIView!
    @IBOutlet weak var sevenKeyView: UIView!
    @IBOutlet weak var eightKeyView: UIView!
    @IBOutlet weak var nineKeyView: UIView!
    @IBOutlet weak var ceroKeyView: UIView!
    @IBOutlet weak var separatorKeyView: UIView!
    @IBOutlet weak var deleteKeyView: UIView!
    @IBOutlet weak var deleteUILabel: UILabel!
    
    @IBOutlet weak var currentCurrencyLabel: UILabel!
    @IBOutlet weak var usdTextField: UITextField!
    
    @IBOutlet weak var convertCurrencyLAbel: UILabel!
    @IBOutlet weak var convertTextField: UITextField!
    
    
    //View model
    var vm: CurrencyViewmodelProtocol!
    var usdNumberFormatter: NumberFormatter!
    var convertionNumberFormatter: NumberFormatter!
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        UIApplication.shared.statusBarStyle = preferredStatusBarStyle
        self.title = "Currency converter"
        self.navigationController?.navigationBar.barStyle = .black
        
        
        //View model
        vm = CurrencyViewModel()
        vm.set(input: self)
        //Layout
        layoutKeyViews()
        rxBinding()
    }
    
    private func rxBinding() {
        vm.currentAmountDriver
            .drive(usdTextField.rx.text)
            .disposed(by: disposeBag)
        
        vm.exchangeAmountDriver
            .drive(convertTextField.rx.text)
            .disposed(by: disposeBag)
        
        Observable.merge([
            oneKeyView.rx.tapGesture().when(.recognized),
            twoKeyView.rx.tapGesture().when(.recognized),
            threeKeyView.rx.tapGesture().when(.recognized),
            fourKeyView.rx.tapGesture().when(.recognized),
            fiveKeyView.rx.tapGesture().when(.recognized),
            sixKeyView.rx.tapGesture().when(.recognized),
            sevenKeyView.rx.tapGesture().when(.recognized),
            eightKeyView.rx.tapGesture().when(.recognized),
            nineKeyView.rx.tapGesture().when(.recognized),
            ceroKeyView.rx.tapGesture().when(.recognized),
            ])
            .map{$0.view?.tag}
            .filter { $0 != nil }
            .map { $0! - 1 }
            .bind(to: nextNumberSubject)
            .disposed(by: disposeBag)
        
        separatorKeyView.rx.tapGesture().when(.recognized)
            .map{_ in ()}
            .bind(to: addSeparatorSubject)
            .disposed(by: disposeBag)
        
        deleteKeyView.rx.tapGesture().when(.recognized)
            .map{_ in ()}
            .bind(to: deleteNumberSubject)
            .disposed(by: disposeBag)
    }

    ///Layout key views
    private func layoutKeyViews(){
        let borderColor = UIColor.lightGray.cgColor
        let borderWith: CGFloat = 0.5
        
        //one
        oneKeyView.layer.borderColor = borderColor
        oneKeyView.layer.borderWidth = borderWith
        //two
        twoKeyView.layer.borderColor = borderColor
        twoKeyView.layer.borderWidth = borderWith
        //three
        threeKeyView.layer.borderColor = borderColor
        threeKeyView.layer.borderWidth = borderWith
        //four
        fourKeyView.layer.borderColor = borderColor
        fourKeyView.layer.borderWidth = borderWith
        //five
        fiveKeyView.layer.borderColor = borderColor
        fiveKeyView.layer.borderWidth = borderWith
        //six
        sixKeyView.layer.borderColor = borderColor
        sixKeyView.layer.borderWidth = borderWith
        //seven
        sevenKeyView.layer.borderColor = borderColor
        sevenKeyView.layer.borderWidth = borderWith
        //eith
        eightKeyView.layer.borderColor = borderColor
        eightKeyView.layer.borderWidth = borderWith
        //nine
        nineKeyView.layer.borderColor = borderColor
        nineKeyView.layer.borderWidth = borderWith
        //cero
        ceroKeyView.layer.borderColor = borderColor
        ceroKeyView.layer.borderWidth = borderWith
        //separator
        separatorKeyView.layer.borderColor = borderColor
        separatorKeyView.layer.borderWidth = borderWith
        //delete
        deleteKeyView.layer.borderColor = borderColor
        deleteKeyView.layer.borderWidth = borderWith
    }
    
    // Perform segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! SelectCurrencyTableViewController
        vc.selectableCurrencyDelegate = self
        vc.currencies = vm.parsedCurrencies
        if segue.identifier == "convertionCurrency"{
            vc.currencyField = CurrencyField.convertion
        }else{
            vc.currencyField = CurrencyField.current
        }
    }
}

extension ViewController: SelectableCurrencyDelegate{
    func select(currency: Currency, for currencyField: CurrencyField) {
        exchangeCurrencySubject.onNext(currency)
    }
}

extension ViewController: CurrencyViewInput {
    var exchangeCurrency: Observable<Currency> {
        return exchangeCurrencySubject
    }
    
    var nextNumber: Observable<Int> {
        return nextNumberSubject
    }
    
    var deleteNumber: Observable<Void> {
        return deleteNumberSubject
    }
    
    var addSeparator: Observable<Void> {
        return addSeparatorSubject
    }
}

