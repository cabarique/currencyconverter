//
//  CurrencyViewModel.swift
//  CurrencyConverter
//
//  Created by Luis Cabarique on 4/26/18.
//  Copyright © 2018 Luis Cabarique. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol CurrencyViewmodelProtocol: CurrencyViewModelOutput {
    func set(input: CurrencyViewInput)
    var parsedCurrencies: [ExchangeCurrency] { get }
}
protocol CurrencyViewModelOutput {
    var currentAmountDriver: Driver<String> { get }
    var exchangeAmountDriver: Driver<String> { get }
}

protocol CurrencyViewInput {
    var disposeBag: DisposeBag { get }
    var nextNumber: Observable<Int> { get }
    var deleteNumber: Observable<Void> { get }
    var addSeparator: Observable<Void> { get }
    var exchangeCurrency: Observable<Currency> { get }
}

typealias ExchangeCurrency = (currency: Currency, value: Double)

struct CurrencyParser {
    var value: Int
    var parsedValue: String
}

class CurrencyViewModel {
    private let client = CurrencyClient.shared
    
    enum config {
        static let exchangeCurrency: Currency = .eur
        static let currentCurrency: Currency = .eur
        static let currentCurrencySymbol: String = "$"
        static let maximumNumbersAllow = 10
        static let currentRate = 1.0
    }
    
    init() {
        client
            .getConversionRates(from: config.currentCurrency)
            .map{$0.rates}
            .subscribe(onSuccess: { [unowned self] rates in
                self.rates.accept(rates)
            })
            .disposed(by: disposeBag)
        
    }
    
    
    //MARK: Outlets
    fileprivate let disposeBag = DisposeBag()
    fileprivate let currentAmount = BehaviorRelay<CurrencyParser>(value: CurrencyParser(value: 0, parsedValue: ""))
    fileprivate let exchangeAmountSubject = PublishSubject<String>()
    fileprivate let exchangeCurrencySubject = BehaviorRelay<Currency>(value: config.exchangeCurrency)
    private let rates = BehaviorRelay<RatesVO?>(value: nil)
    private let currentRate = BehaviorRelay<Double>(value: config.currentRate)
    
    func set(input: CurrencyViewInput) {
        currentAmount
            .subscribe(onNext: {[unowned self] amount in
                let exchangeValue = Double(amount.value) * self.currentRate.value
                let numberFormatter = self.getCurrenyNumberFormatter(currencySymbol: self.exchangeCurrencySubject.value.rawValue.uppercased())
                if let formatterNumber = numberFormatter.string(from: NSNumber(value: exchangeValue)) {
                    self.exchangeAmountSubject.onNext(formatterNumber)
                }
            })
            .disposed(by: disposeBag)
        
        currentRate
            .map{[unowned self] _ in self.parseValue(self.currentAmount.value) }
            .bind(to: currentAmount)
            .disposed(by: input.disposeBag)
        
        input.nextNumber
            .filter{[unowned self] _ in self.rates.value != nil}
            .map{[unowned self] next in
                self.parseValue(self.currentAmount.value, next: String(next))
            }
            .bind(to: currentAmount)
            .disposed(by: input.disposeBag)
        
        let exchangeCurrencyObservable = input.exchangeCurrency.share()
        exchangeCurrencyObservable
            .bind(to: exchangeCurrencySubject)
            .disposed(by: input.disposeBag)
        
        exchangeCurrencyObservable
            .map { [unowned self] currency -> Double? in
                guard let rates = self.rates.value else { return nil }
                return self.getRate(from: currency, from: rates)
            }
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: currentRate)
            .disposed(by: input.disposeBag)
        
        input.deleteNumber
            .filter{[unowned self] _ in self.rates.value != nil}
            .map{[unowned self] next in
                let currentValue = self.currentAmount.value
                guard String(currentValue.value).count > 1
                    else { return CurrencyParser(value: 0, parsedValue: "") }
                let numberFormatter = self.getCurrenyNumberFormatter(currencySymbol: config.currentCurrencySymbol)
                guard
                    let nextInt = Int("\(currentValue.value)".dropLast()),
                    let formatterNumber = numberFormatter.string(from: NSNumber(value: nextInt))
                    else { return currentValue }
                
                return CurrencyParser(value: nextInt, parsedValue: formatterNumber)
            }
            .bind(to: currentAmount)
            .disposed(by: input.disposeBag)
    }
    
    
}

private extension CurrencyViewModel {
    
    func parseValue(_ amount: CurrencyParser, next: String = "") -> CurrencyParser{
        let numberFormatter = self.getCurrenyNumberFormatter(currencySymbol: config.currentCurrencySymbol)
        guard
            let nextInt = Int("\(amount.value)\(next)"),
            let formatterNumber = numberFormatter.string(from: NSNumber(value: nextInt))
            else { return amount }
        
        return CurrencyParser(value: nextInt, parsedValue: formatterNumber)
    }
    
    func getCurrenyNumberFormatter(currencySymbol: String) -> NumberFormatter{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.alwaysShowsDecimalSeparator = false
        numberFormatter.currencySymbol = currencySymbol
        numberFormatter.allowsFloats = true
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 2
        return numberFormatter
    }
    
    /**
     get a rate from currency value object
     */
    func getRate(from currency: Currency, from rates: RatesVO) -> Double {
        let rates = rates.rateDictionary
        var rateValue = 1.0
        for rate in rates{
            if rate.key == currency.rawValue{
                rateValue = rate.value
            }
        }
        return rateValue
    }
}

extension CurrencyViewModel: CurrencyViewmodelProtocol {
    var parsedCurrencies: [ExchangeCurrency] {
        let currentAmount = self.currentAmount.value.value
        guard let rates = rates.value else { return [] }
        return rates.rateDictionary.compactMap { value in
            let convertedAmount = Double(currentAmount) * value.value
            guard let currency = Currency(rawValue: value.key) else { return nil }
            return (currency, convertedAmount)
        }
    }
    
    var exchangeAmountDriver: Driver<String> {
        return exchangeAmountSubject.asDriver(onErrorDriveWith: Driver.never())
    }
    
    var currentAmountDriver: Driver<String> {
        return currentAmount.map{$0.parsedValue}.asDriver(onErrorDriveWith: Driver.never())
    }
    
    
}
