//
//  SelectCurrencyTableViewController.swift
//  CurrencyConverter
//
//  Created by Luis Cabarique on 4/28/18.
//  Copyright © 2018 Luis Cabarique. All rights reserved.
//

import UIKit

protocol SelectableCurrencyDelegate: class{
    func select(currency: Currency, for currencyField: CurrencyField)
}

class SelectCurrencyTableViewController: UITableViewController {
    
    var currencies: [ExchangeCurrency] = []
    var currencyField: CurrencyField!
    var selectableCurrencyDelegate: SelectableCurrencyDelegate?
    
    var currenyNumberFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.alwaysShowsDecimalSeparator = false
        numberFormatter.allowsFloats = true
        numberFormatter.currencySymbol = ""
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 2
        return numberFormatter
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currency", for: indexPath)

        let currency = currencies[indexPath.row]
        cell.textLabel?.text = currency.currency.rawValue
        let value = currenyNumberFormatter.string(from: NSNumber(value: currency.value))
        cell.detailTextLabel?.text = value
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currency = currencies[indexPath.row]
        selectableCurrencyDelegate?.select(currency: currency.currency, for: currencyField)
        self.navigationController?.popViewController(animated: false)
    }

}
