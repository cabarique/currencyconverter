//
//  CurrencyService.swift
//  CurrencyConverter
//
//  Created by Luis Cabarique on 4/26/18.
//  Copyright © 2018 Luis Cabarique. All rights reserved.
//

import Foundation
import Moya

enum CurrencyEndPoints {
    case getConversionRates(base: String)
}

extension CurrencyEndPoints: TargetType{
    var baseURL: URL {
        return URL(string: "http://data.fixer.io/api/latest")!
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getConversionRates(_):
            //Base is not longer supported by free plan
            return .requestParameters(parameters: [
                "access_key": "3d8621aec15431100cca883fefcdd22a"], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
}
