//
//  CurrencyClient.swift
//  CurrencyConverter
//
//  Created by Luis Cabarique on 4/26/18.
//  Copyright © 2018 Luis Cabarique. All rights reserved.
//

import Foundation
import Moya
import RxSwift

class CurrencyClient {
    
    static let shared = CurrencyClient()
    
    private let currencyAPI: MoyaProvider<CurrencyEndPoints>
    
    init() {
        currencyAPI = MoyaProvider<CurrencyEndPoints>()
    }
    
    func getConversionRates(from currency: Currency) -> Single<CurrencyVO>{
        let currencyString = currency.rawValue
        return currencyAPI.rx.request(.getConversionRates(base: currencyString)).map(CurrencyVO.self)
    }
}
