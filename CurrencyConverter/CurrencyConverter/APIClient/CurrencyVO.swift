//
//  CurrencyVO.swift
//  CurrencyConverter
//
//  Created by Luis Cabarique on 4/26/18.
//  Copyright © 2018 Luis Cabarique. All rights reserved.
//

import Foundation

enum Currency: String, Encodable{
    case usd = "USD"
    case aud = "AUD"
    case bng = "BGN"
    case brl = "BRL"
    case cad = "CAD"
    case chf = "CHF"
    case cny = "CNY"
    case czk = "CZK"
    case dkk = "DKK"
    case eur = "EUR"
    case gbp = "GBP"
    case hkd = "HKD"
    case hrk = "HRK"
    case huf = "HUF"
    case idr = "IDR"
    case ils = "ILS"
    case inr = "INR"
    case isk = "ISK"
    case jpy = "JPY"
    case krw = "KRW"
    case mxn = "MXN"
    case myr = "MYR"
    case nok = "NOK"
    case nzd = "NZD"
    case php = "PHP"
    case pln = "PLN"
    case ron = "RON"
    case rub = "RUB"
    case sek = "SEK"
    case sgd = "SGD"
    case thb = "THB"
    case Try = "TRY"
    case zar = "ZAR"
    
    static var all: [Currency] = [
        .usd, .aud, .bng, .brl, .cad, .chf, .cny, .czk, .dkk, .eur, .gbp, .hkd, .huf,
        .idr, .ils, .inr, .isk, .jpy, .krw, .mxn, .myr, .nok, .nzd, .php, .pln, .ron,
        .rub, .sek, .sgd, .thb, .Try, .zar
    ]
}

struct CurrencyVO: Decodable {
    let base: String
    let date: String
    let rates: RatesVO
    
    
}

struct RatesVO: Codable{
    let USD: Double?
    let AUD: Double?
    let BGN: Double?
    let BRL: Double?
    let CAD: Double?
    let CHF: Double?
    let CNY: Double?
    let CZK: Double?
    let DKK: Double?
    let EUR: Double?
    let GBP: Double?
    let HKD: Double?
    let HRK: Double?
    let HUF: Double?
    let IDR: Double?
    let ILS: Double?
    let INR: Double?
    let ISK: Double?
    let JPY: Double?
    let KRW: Double?
    let MXN: Double?
    let MYR: Double?
    let NOK: Double?
    let NZD: Double?
    let PHP: Double?
    let PLN: Double?
    let RON: Double?
    let RUB: Double?
    let SEK: Double?
    let SGD: Double?
    let THB: Double?
    let TRY: Double?
    let ZAR: Double?
    
    var data: Data {
        return try! JSONEncoder().encode(self)
    }
    
    var rateDictionary: [String: Double]{
        return (try? JSONSerialization.jsonObject(with: data)) as? [String: Double] ?? [:]
    }
}
